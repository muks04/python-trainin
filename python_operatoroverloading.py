class Height:
    def __init__(self, ft, inch):
        self.ft = ft
        self.inch = inch

    def __str__(self):
        return '%d ft: %d inch ' % (self.ft, self.inch)

    def __sub__(self, other):
        inch = self.inch - other.inch
        if inch >= 12:
            ft = self.ft - other.ft - 1
            inch = inch - 12
        else:
            ft = self.ft - other.ft
        return Height(ft, inch)


a = Height(10, 3)
b = Height(2, 5)
print(a)
print(b)
c = a-b
print(c)
