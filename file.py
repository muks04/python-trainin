sample_file = open('hello.txt','w')

sample_file.write('hello world,it will be in file.')
sample_file.close()

with open('next_hello.txt','w')as sample_file_1: #with will close the file it self
    sample_file_1.write('hello world,it will be in file.')

print('hello')

with open('excel1.csv','r') as student_file:
    for each_line in student_file:
        print(each_line)