names = ['harry kumar bhusal', 'santosh  khanal', 'pujan raj gautam', 'gourav bhahadhur kandel',
         'kale  kanxa']
fixed_names = []
surname = []
firstname = []
for name in names:
    parts = name.split(' ')
    surname.append(parts[-1])
    firstname.append((parts[0]))
print(fixed_names)
print(surname)
print(firstname)

# q2
names = ['harry kumar bhusal', 'santosh khanal', 'pujan raj gautam', 'gourav bhahadhur kandel',
         'kale kanxa']
with_middle_name = []
without_middle_name = []
for name in names:
    parts = name.split(' ')  # print('parts=',parts)
    if len(parts) == 3:
        with_middle_name.append(name)
    else:
        without_middle_name.append(name)

print('the name with middle name are:', with_middle_name)
print('the name without middle name are:', without_middle_name)


#reversing the name

print('\n' * 3)
names = ['harry kumar bhusal', 'santosh khanal', 'pujan raj gautam', 'gourav bhahadhur kandel',
         'kale kanxa']
reverse_name = []
for name in names:
    parts = name.split(' ')
    reverse_name.append(' '.join(parts[::-1]))  #::-1 wil reverse
print(reverse_name)
