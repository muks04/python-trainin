from random import randint

# create a list of play option

t = ['r', 'p', 's']  # t is a list

# assign a random play to capture

computer = t[randint(0, 2)]

# set a player to false
player = False

while player == False:
    print(computer)
    # set player to true
    player = input("'rock', 'paper', 'scissors' :")
    if player == computer:
        print("tie")
    elif player == 'r':
        if computer == 'p':
            print("you lose", computer, "covers", player)
        else:
            print("you win", player, "smashes", computer)
    elif player == 'p':
        if computer == 's':
            print("you loose", computer, "cut", player)
        else:
            print("you win", player, "covers", computer)
    elif player == 's':
        if computer == 'r':
            print("you loose", computer, "smashes", player)
        else:
            print("you win", player, "cut", computer)
    else:
        print("that's not a valid play. check your spelling")

# player was set to true.but wee want it to be false so the loop continues
player = False
computer = t[randint(0, 2)]
