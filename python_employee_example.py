class Employee:
    Name = None
    Gender = None
    Address = None
    Mobile = None
    Married = None
    Yearly_income = None

    def calculate_tax(self):
        if self.Married == True and self.Yearly_income > 4500000:
            taxable_amount = self.Yearly_income - 4500000
            return taxable_amount * 0.15
        elif self.Married == False and self.Yearly_income > 3500000:
            taxable_amount = self.Yearly_income - 4500000
            return taxable_amount * 0.15
        else:
            return self.Yearly_income * 0.01


Mukesh = Employee()
Mukesh.Name = 'Mukesh Maharjan'
Mukesh.Address = 'Harisiddhi'
Mukesh.Mobile = '9861203234'
Mukesh.Married = False
Mukesh.Yearly_income = 650000

print("Name:", Mukesh.Name)
print("Address:", Mukesh.Address)
print("Mobile:", Mukesh.Mobile)
print("Martial status:", Mukesh.Married)
print("Yearly income is:", Mukesh.Yearly_income)
print("Tax:", Mukesh.calculate_tax())