# dictionary is used for  giving index & use key and value
marks = {'eng': 50, 'nep': 55, 'hpe': 57}
total = 0

del marks['eng']  # deleting value

marks['math'] = 80  # inserting value

print(marks['hpe'])



for key in marks:  # for loop
    total += marks[key]
    percentage =  round(total/3,2)
    print(key, marks[key])
    print('total value is :', total)
    print('percentage is :', percentage)