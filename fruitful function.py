def fruitful_even_or_odd(no):
    if no % 2 == 0:
        return True
    else:
        return False


def void_even_or_odd(no):
    if no % 2 == 0:
        print('Even')
    else:
        print('Odd')


# print(fruitful_even_or_odd(9))
# void_even_or_odd(8)


def fruitful_add(a, b):
    return (a + b)


def void_add(a, b):
    print(a + b)


c = fruitful_add(4, 7)
print(c)
d = void_add(5, 7)
print(d)

entered_number = input('enter a number')

if void_even_or_odd(int(entered_number)):
    print('thankyou for entering even number ')

else:
    print('please try again')
