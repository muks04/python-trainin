# *
# **
# ***
# ****
# *****
print('\number 1.')
print('\n' * 2)
for i in range(1, 6):
    print('*' * i)

# ******
# *****
# ****
# ***
# **
# *

print('\number 2.')
print('\n' * 2)
for i in range(6, 0, -1):
    print('*' * i)

#      *
#     **
#    ***
#   ****
#  *****
print('\n' * 2)
print('\number 3.')
for i in range(6, 0, -1):
    print(' ' * i, '*' * (6 - i))

# ******
#  *****
#   ****
#    ***
#     **
#      *

print('\n' * 2)
print('\number 4.')
for i in range(6, 0, -1):
    print(' ' * (6 - i), '*' * i)

print('\n' * 2)
print('\number 5.')
for i in range(6):
    print((6 - i) * ' ' + (2 * i + 1) * '*')
for i in range(6, -1, -1):
    print((6 - i) * ' ' + (2 * i + 1) * '*')

print('\n' * 2)

max_number_of_stars = 27

space_count = max_number_of_stars // 2
star_count = 1
for i in range(max_number_of_stars):
    print(' ' * space_count + '*' * star_count)

    if i < max_number_of_stars // 2:
        space_count -= 1
        star_count += 2
    else:
        space_count += 1
        star_count -= 2

print('\nnumber 6.')

print('\n' * 2)

max_number_of_stars = 15

space_count = max_number_of_stars // 2
star_count = 1
for i in range(max_number_of_stars):
    if star_count - 2 > 0:
        print(' ' * space_count + '*' + ' ' * (star_count - 2) + '*')
    else:
        print(' ' * space_count + '*')

    if i < max_number_of_stars // 2:
        space_count -= 1
        star_count += 2
    else:
        space_count += 1
        star_count -= 2

print('\nnumber 8.')
print('\n' * 2)
max_number_of_stars = 15

space_count = max_number_of_stars // 2
star_count = 1
for i in range(max_number_of_stars):
    if star_count - 2 > 0:
        print(' ' * (star_count - 2) + '*')
    else:
        print('*')

    if i < max_number_of_stars // 2:
        space_count -= 1
        star_count += 2
    else:
        space_count += 1
        star_count -= 2

print('\nnumber 8.')
print('\n' * 2)
max_number_of_stars = 15

space_count = max_number_of_stars // 2
star_count = 1
for i in range(max_number_of_stars):
    if star_count - 2 > 0:
        print(' ' * space_count + '*' + ' ' * (star_count - 2))
    else:
        print(' ' * space_count)

    if i < max_number_of_stars // 2:
        space_count -= 1
        star_count += 2
    else:
        space_count += 1
        star_count -= 2

print('\nnumber 9.')
print('\n' * 2)
n = input('Enter the string : ')
for i in range(len(n)):
    for j in range(i + 1):
        if j % 2 == 0:
            print(n[j].upper(), end="")
        else:
            print(n[j].lower(), end='')

    print()
