my_diff = lambda a,b:a-b
print(my_diff(10,5))

rev = lambda string:string[::-1]
print(rev('mukesh'))

capital = lambda  s:s.capitalize()
print(capital('mukesh'))

palindrome =lambda str:str ==str[::-1]
print(palindrome('aba'))


import math
hypotenous =  lambda  p,b:math.sqrt(p**2+b**2)
print('hypotenous=',hypotenous(3,4))


#mean
mean = lambda sample_list:sum(sample_list)/len(sample_list)
print('mean',mean([4,5,6]))

#reverse
names = ['john','josh','dave']
reverse_names = [] #['nhoj',.....]
for name in names:
    reverse_names.append(name[::-1])
print(reverse_names)



reverse_string = lambda s: s[::-1]
new_way =map(reverse_string,names)
print(list(new_way))

#new_way
new_way  = map(lambda s: s[::-1],names)
print(list(new_way))

num=[2,3,4,5]
def square_number(n):
    return n**2
square_num=map(square_number,num)
print(list(square_num))

square=map(lambda s: s**2,num)
print(list(square))


#adding mr in names
names = ['Ram','Muks','Apex']
new_name = map(lambda s: "Mr"+s,names)
print(list(new_name))


total_marks = [750,659,450,720,700]
percent=map(lambda s: s/8,total_marks)
# print(list(percent))

data=list(percent)

def grade(percent):
    if percent >= 80:
        return 'distinction'
    elif percent >= 60:
        return 'first'
    elif percent >= 40:
        return 'second'
    elif percent < 40:
        return 'fail'
    return percent
grade_result =map(grade,data)
print(list(grade_result))


num = [2,3,4,5]
new_num = map(lambda s:s**-1,num)
print(list(new_num))


