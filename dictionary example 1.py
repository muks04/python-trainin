# we will find no of fail an email
# note: each student is dictionary

student = [
    {'name': 'hari', 'email': 'hari@hotmail.com', 'percentage': 65},
    {'name': 'hira', 'email': 'hira@gmail.com', 'percentage': 32},
    {'name': 'sita', 'email': 'sita@hotmail.com', 'percentage': 70},
    {'name': 'ram', 'email': 'ram@yahoo.com', 'percentage': 33}

]

fail_count = 0

gmail_count = 0
hotmail_count =0
for student in student:
    print('is a single student', student)
    if student['percentage'] < 40:
        fail_count += 1
    parts = student['email'].split('@') #parts dividing email address aftr @
    if parts[-1] == 'gmail.com':
        gmail_count += 1
    if parts[-1] == 'hotmail.com':
        hotmail_count += 1

print('fail count is :', fail_count)
print('gmail user count is :',gmail_count)
print('hotmail user count is :',hotmail_count)