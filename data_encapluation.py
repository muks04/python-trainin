class Customer:
    def __init__(self,name,balance,address,account_type):
        self.name = name
        self.__balance =balance
        self.address = address
        self.account_type = account_type

    def deposite(self,amount):
        self.__balance = self.__balance + amount

    def withdraw(self,amount):
        if self.__balance >= amount:  #__balance wile make balance private
            self.__balance = self.__balance - amount
        else:
            print("Insufficent balance !!!!")

    def print__balance(self):
        print("Balance = ",self.__balance)


Mukesh = Customer('Mukesh',510000,'Harisiddhi','Saving')
Mukesh.print__balance()
Mukesh.deposite(10000000)
Mukesh.print__balance()
Mukesh.balance = 0
Mukesh.print__balance()
Mukesh.deposite(5000000)
Mukesh.print__balance()
Mukesh.withdraw(2000000)
Mukesh.print__balance()

