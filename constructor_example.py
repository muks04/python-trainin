class Employee:
    def calculate_tax(self):
        return self.salary * 0.01

    def __init__(self, name, address, salary, ):
        self.name = name
        self.address = address
        self.salary = salary


class Teacher(Employee):
    def __init__(self, name, address, salary, full_time, subject):
        super().__init__(name, address, salary)

        self.full_time = full_time
        self.subject = subject

    def take_class(self):
        print('class taken')


class Helper(Employee):
    def __init__(self, name, address, salary):
        super().__init__(name, address, salary)
        self.name = name
        self.address = address
        self.salary = salary


    def clean_desk(self):
        print('clean desk')


class Manager(Employee):
    def __init__(self, name, address,salary,department):
        super().__init__(name,address,salary)
        self.department = department




    def task_assigned(self):
        print('Task Assigned')


sample_teacher = Teacher('Mukesh Maharjan', 'Harisiddhi', True, 200000, 'Python')
sample_helper = Helper('Pujan Gautam', 'Dhapalhel', 100000)
sample_manager = Manager('Santosh Khanal', 'Science', 300000, 'syangha')
sample_teacher.take_class()
print(" Task amoount of teacher is : ", sample_teacher.calculate_tax())
sample_helper.clean_desk()
print(" Task amount of helper is : ", sample_helper.calculate_tax())
sample_manager.task_assigned()
print(" Task amount of manager is : ", sample_manager.calculate_tax())
