# standard deviation
import math

A = [4, 10, 14, 13, 14]
total = 0
mean = sum(A) / len(A)
for item in A:
    total = total + (item - mean) ** 2
std = math.sqrt((total / len(A)))

print('mean is : ', mean)
print('standard deviation is :', std)

A.sort()
print('sorted data is : ',A)
if len(A) % 2 == 0:
    median = len(A) / 2
else:
    median = (len(A) + 1) / 2
print('median is : ', median)
print('median is : ', A[int(median) - 1])
