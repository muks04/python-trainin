for i in range(100):
    print('hello world', i)

    if i == 50:
        break


def prime_or_composite(num):
    prime = True
    for i in range(2, num):  # (2,n-1)
        if num % i == 0:
            prime = False
            break

    return prime


print('prime number')

num = int(input('enter the number : '))
print(prime_or_composite(num))

#ans : true prie number false coposite number