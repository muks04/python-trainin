def another_func(*args, **kwargs):
    print(args)
    print(kwargs)

another_func(1,2,3,'ram',name='hari', roll_no=45)
another_func()

#alias
A = [3,4,5,6]
B = A
D = A.copy()
C = [3,4,5,6]
#B.append(8) #for alias

print(A)
print(B)
print(B == C)
print(A == C)
print(A is B) #to check alias
print(A is C)
print(D)

# in operator is used for searching in string