class Time:
    def __init__(self, hour, min, sec):
        self.hour = hour
        self.min = min
        self.sec = sec

    def __str__(self):
        return '%d hour: %d min : %d sec ' % (self.hour, self.min, self.sec)

    def __add__(self, other):
        sec = self.sec + other.sec
        if sec >= 60:
            min = self.min + other.min + 1
            sec = sec - 60
        else:
            min = self.min + other.min

        if min >= 60:
            hour = self.hour + other.hour+1
            min = min - 60
        else:
            hour = self.hour + other.hour
      
        return Time(hour, min, sec)


a = Time(10, 35, 40)
b = Time(2, 30, 5)
print(a)
print(b)
c = a + b
print(c)
