names = ["ram bd maharjan", "sita pd shrestha", "hari dangol", "muks maharjan"]
name_without_middle_name = filter(lambda name: len(name.split(' ')) == 2, names)
print(list(name_without_middle_name))

name_with_middle_name = filter(lambda name: len(name.split(' ')) == 3, names)
print(list(name_with_middle_name))

import math

sample_list = [1, 3, 5, 4, 25, 64, 34, 65]
perfect_square = filter(lambda n: math.sqrt(n) == int(math.sqrt(n)), sample_list)
print(list(perfect_square))

multiple_of_5 = filter(lambda n: n % 5 == 0, sample_list)
print(list(multiple_of_5))

multiple_of_3 = filter(lambda n: n % 3 == 0, sample_list)
print(list(multiple_of_3))

location = ['kathmandu,np', 'lumbini,np', 'goa,ind', 'bengal,ind']


def loc(elem):
    a = elem.split(',')
    if a[-1] == 'np':
        return a


res = filter(loc, location)
print(list(res))

res1 = filter(lambda n: n.split(',')[-1] == 'ind', location)
print(list(res1))

name = ['santosh ali baba lal khan', 'pujan bahadur taklu boss', 'gourav sabai vanda thulo tauko']
res2 = filter(lambda n: len(n) > 15, name)
print(list(res2))

# arstron=[1,2,4,77,45,153,87,77,370,371,407]
# res3=filter(lambda n:
#

num = [-1, -5, -10, 5, 7, 8, 9, 3]
neg = filter(lambda n: n < 0, num)
print(list(neg))

namess = ['ram', 'shyam', 'hzz', 'auks', 'eukesh', 'iuna']
vowelans = filter(lambda n: n[0] == 'a', namess)
print(list(vowelans))






