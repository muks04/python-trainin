import os

BASE_DIR = r'C:\Users\mukspc\Desktop\random'  # r means raw string
print(os.listdir(BASE_DIR))
# relative path eg:('DSC00472.JPG',)

# fullpath  eg:(C:\Users\mukspc\Desktop\random')


file_types = {
    'photos': ['png', 'JPG', 'JPEG', 'gif'],
    'documents': ['key', 'ppt', 'pptx', 'xls', 'pdf', 'csv', 'docx'],
    'movie': ['mp4', 'mkv', 'avi', 'mpeg'],
    'tetx_file': ['txt'],
    'python': ['pyc', 'py']

}


# for file_type in file_types:
#     print('only key', file_type)
# print(os.listdir(BASE_DIR))



# for file_type in file_types:
#     full_path =os.path.join(BASE_DIR,file_type)  #to make code os independdent ##(os.path) is used
#     print(full_path)
#     print('only key',file_type)
# print(os.listdir(BASE_DIR))


# #to make folder
# for file_type in file_types:
#     full_path = os.path.join(BASE_DIR, file_type)  # to make code os independdent ##(os.path) is used
#     print(full_path)
#     os.mkdir(full_path)


# #to make folder is the folder name is already made
# for file_type in file_types:
#     full_path = os.path.join(BASE_DIR, file_type)
#     if not os.path.exists(full_path):
#        os.mkdir(full_path)


# #logic for moving files to folder
# for file in os.listdir(BASE_DIR):
#     print(file)

# to print extension
# for file in os.listdir(BASE_DIR):
#     extension = file.split('.')[-1]
#     print(extension)



#checking each files with each types
#
# for file in os.listdir(BASE_DIR):
#     extension = file.split('.')[-1]
#     for key in file_types:
#         print(file,key,file_types[key])
#         if extension in file_types[key]:
#             print(extension, 'is a', key)


#giving full path to the file and puting then in  their file
#BASE_DIR=C:\Users\mukspc\Desktop\random\   file= excel1112.csv  destibation (key)  =documents
for file in os.listdir(BASE_DIR):
    extension = file.split('.')[-1]
    for key in file_types:
        if extension in file_types[key]:
            print(extension, 'is a', key)
            path_src = os.path.join(BASE_DIR,file)
            path_dest = os.path.join(BASE_DIR,key,file)
            print(path_src,path_dest)
            os.rename(path_src,path_dest)