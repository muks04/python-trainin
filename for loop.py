# for loop

for i in range(10):
    print(i)

for j in range(1, 15):
    print(j)

for k in range(10, 20, 2):
    print(k)

for l in range(1, 101):
    print(l)

for m in range(2, 101, 2):
    print(m)

for n in range(1, 101, 2):
    print(n)

for o in range(5, 101, 5):
    print(o)

for number in range(1, 11):
    print('6*', number, ' = ', 6 * number)

s = 0
for i in range(1, 101):
    s = s + i
print(s)

# factorial
fact = 1
num = int(input('enter the number'))
for number in range(1, num + 1):
    fact *= number
print('factorial is', fact)

fact = 1
for i in range(1, 6):
    fact = fact * i
print('factorial is', fact)

