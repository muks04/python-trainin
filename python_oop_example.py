import math


class Rectangle():
    def __init__(self, l, b):
        self.l = l
        self.b = b

    def area(self):
        return self.l * self.b

    def perimeter(self):
        return 2 * (self.l + self.b)

    def square(self):
        if self.l == self.b:
            return 'square'

    def diagonal(self):
        return math.sqrt(self.l ** 2 + self.b ** 2)


sample_rectangel = Rectangle(3, 4)
print("Area is : ", sample_rectangel.area())
print("Perimeter is : ", sample_rectangel.perimeter())
print("Rectangle is : ", sample_rectangel.square())
print("Rectangle is : ", sample_rectangel.diagonal())

# triangle
import math


class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def area(self):
        s = (self.a + self.b + self.c) / 2
        return math.sqrt(s * (s - self.a) * (s - self.b) * (s - self.c))


sample_triangle = Triangle(3, 4, 5)
print("Area is : ", sample_triangle.area())


class Rectangle():
    l = None
    b = None

    def area(self):
        return self.l * self.b

    def perimeter(self):
        return 2 * (self.l + self.b)

    def square(self):
        if self.l == self.b:
            return 'square'

    def diagonal(self):
        return math.sqrt(self.l ** 2 + self.b ** 2)

    def equilater(self):
        return self.a == self.b == self.c

    def isosceles(self):
        if self.a == self.b or self.b == self.c or self.a == self.c:
            return 'isosceles'
        else:
            return False


sample_rectangel = Rectangle()
sample_rectangel.l = 3
sample_rectangel.b = 4
print("Area is : ", sample_rectangel.area())
print("Perimeter is : ", sample_rectangel.perimeter())
print("Rectangle is : ", sample_rectangel.square())
print("Rectangle is : ", sample_rectangel.diagonal())

# triangle
import math


class Triangle():
    a = None
    b = None
    c = None

    def area(self):
        s = (self.a + self.b + self.c) / 2
        return math.sqrt(s * (s - self.a) * (s - self.b) * (s - self.c))


sample_triangle = Triangle()
sample_triangle.a = 3
sample_triangle.b = 4
sample_triangle.c = 5
print("Area is : ", sample_triangle.area())

# circle
import math


class Circle():
    r = None

    def area(self):
        return math.pi * self.r ** 2

    def circumference(self):
        return 2 * math.pi * self.r

    def diagonal(self):
        return 2 * self.r


sample_circle = Circle()
sample_circle.r = 7
print("Area is : ", sample_circle.area())
print("ciecumference is : ", sample_circle.circumference())
print("diagonal is : ", sample_circle.diagonal())
